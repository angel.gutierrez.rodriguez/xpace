## Angel Gutierrez Rodriguez
## 2003 - 2004 - Karolinska Institutet - Stockholm - Sweden
## 2005 - 2007 - CSIC - Madrid - Spain
###############################################################################
## Xpace  ##  2006 # Angel Gutierrez Rodriguez
## EXpace ##  2007 # Angel Gutierrez Rodriguez
global xpace_version,expace_version
xpace_version ='v 1 e 7'
# 1.7 XML save.
expace_version='v 2 e 2'
# 1.0 Tkinter GUI with menu.
# 2.0 Mnu replace by tabs.
# 2.2 XML added. Remodele Input/Output tab.
###############################################################################
## Touched by the hand of AGR...
###############################################################################
## General modules for Xpace
from math import *
from copy import *
from time import *           # Just for dates.
## General modules for EXpace
from Tkinter        import * # Tkinter modules
from tkMessageBox   import *
import tkFileDialog,tkFont
import Pmw
##
##  The Matrix class
##
class Matrix:
  '''A class to store mathematical 2 dimensional matrices'''
  def __init__(self,l,d=[0,0]):
    '''Input the matrix components.'''
    if type(l[0])==type([1,1]):          # Square form
      self.mlsq=l
      self.dim=[len(self.mlsq[0]),len(self.mlsq)]
      self.ml=[]              # Generates also the list representation
      for i in self.mlsq:
        for j in i: self.ml.append(j)
    else:                               # Lineal form
      for i in d:             # Checking the dimension information
        if i <=0: return 'ERROR: No dimension specified.'
      self.dim=d
      self.ml=l
      temp=[]
      self.mlsq=[]
      for i in self.ml:
        if len(temp)<self.dim[0]: temp.append(i)
        else:
          self.mlsq.append(temp)
          temp=[]
          temp.append(i)
      self.mlsq.append(temp)
  def sq(self):
    '''Returns the squared (list of lists) form of the matrix.'''
    return self.mlsq
  def lin(self):
    '''Returns the lineal form of the matrix.'''
    return self.ml
  def det(self):
    '''Calculates the determinant of the matrix. Recursive, of course!'''
    if (self.dim[0]!=self.dim[1]):
      print 'ERROR: Not a square matrix'
      return -1
    elif self.dim[0]==1: return int(self.ml[0])  # One dimension matrix
    else:
      res=0.0
      for i in xrange(self.dim[0]):
        adj=[]
        for j in self.mlsq[1:]:
          temp=[]
          for k in xrange(len(j)):
            if (k!=i): temp.append(j[k])
          adj.append(temp)
          if fmod(i,2)==0: exp= 1.0
          else:            exp=-1.0
          adj=Matrix(adj)
        res=res+self.mlsq[0][i]*exp*adj.det()
      return res
##
##  The MSystem class
##
class MSystem:
    '''Mathematical equation system.'''
    def __init__(self,a,b):
        ''' A=Matrix class B=list.'''
        self.a=a
        self.b=b
        self.n=self.dim()
    def dim(self):  return len(self.b)
    def adet(self): return float(self.a.det())
    def solve(self):
        '''Cramer s method. Slow for fig sizes (more than 4)'''
        det=self.adet()
        if det==0.0: 
 	    print 'ERROR: No solution. Det(A)=0.'
	    return []
        else:
            res=[]
            for var in xrange(self.n):
                ax=[]
                temp=[]
                for j in xrange(self.n):
                    for k in xrange(self.n):
                        if var==k: temp.append(self.b[j])
                        else:      temp.append(self.a.mlsq[j][k])
                    ax.append(temp)
                    temp=[]
                a2=Matrix(ax)
                res.append(a2.det()/det)
            return res
    def set_ext(self):
        '''Systems extended matrx.'''
        x=[]
        for i in xrange(len(self.a.mlsq)):
            z=self.a.mlsq[i]
            z.append(self.b[i])
            x.append(z)            
        return Matrix(x)          
    def solve2(self):
        '''Gauss elimination'''
        extmat=self.set_ext()
        for k in xrange(0,extmat.dim[1]-1):
            for i in xrange(k+1,extmat.dim[1]):
                if extmat.mlsq[i][k]==0.0: pass
                else:
                    a=float(extmat.mlsq[k][k])/extmat.mlsq[i][k]
                    for j in xrange(len(extmat.mlsq[0])):
                        extmat.mlsq[i][j]=extmat.mlsq[i][j]*a-extmat.mlsq[k][j]
        sol=[]
        for i in xrange(extmat.dim[1]-1,-1,-1):
            a=extmat.mlsq[i][i]
            b=extmat.mlsq[i][extmat.dim[1]]
            u=extmat.mlsq[i][i+1:extmat.dim[1]]
            u.reverse()
            y=0.0
            for i in xrange(len(sol)): y=y+sol[i]*u[i]
            if a==0.0: 
                print 'ERROR: No solution. Det(A)=0.'
                return []
            else: sol.append((b-y)/a)
        sol.reverse()
        return sol
##
##  The Xpace class
##
class Xpace:
    def __init__(self):
        '''All blank.'''
        self.pc=''    # Protein code
        self.pn=''    # Protein name
        self.ec=''    # Experiment code
        self.en=''    # EXteriment name
        self.ver=0    # Script version
        self.evo=0    # Script evolution
        self.dim=[]   # Dimension
        self.unit=[]  # Units
        self.exp=[]   # Experiments
        self.obs=[]   # Scores
## Methods to setup Xpace value
    def proteincode(self,s):   self.pc=s
    def proteinname(self,s):   self.pn=s
    def exp_code(self,s):      self.ec=s
    def exp_name(self,s):      self.en=s
    def version(self,n):       self.ver=n
    def evolution(self,n):     self.evo=n
## 
    def add_dimension(self,s1,s2):
        '''Add dimension with $1=dimension label,$2=dimension unit.'''
        self.dim.append(s1)
        self.unit.append(s2)
        if (len(self.exp)>0):
            for i in self.exp: i.append(0)
        print 'New dimension of name',s1,'and units in',s2,'added.'
    def del_dimension(self,dim_label):
        w=self.dim.index(dim_label)
        y=[]
        for i in self.exp:
            z=[]
            for j in xrange(len(self.dim)):
                if j!=w: z.append(i[j])
            y.append(z)
# WARN: is this ok?
        self.exp=y
        del self.dim[w]
        del self.unit[w]
        print 'Dimension ',dim_label,'deleted.'
##
    def add_exp(self,l):
      '''Add experiment.'''
      if (len(l)==len(self.dim)):
        x=[]
        for i in l: x.append(float(i))
        self.exp.append(x)
        return 0
      else:
        return 'ERROR: Number of dimensions not consistent with the space.'
## 
    def set_obs(self,l,n):
        '''Add observation with $1=experiment coordinates list,
           $2=observation'''
#AGR2005        print l, n,self.exp
        try:
            x=self.exp.index(l)
            n=float(n)
            if (len(self.obs)>x): self.obs[x]=n
            else: self.obs.append(n)
            return 0
        except: return 'ERROR: Experiment not found.'
##  Shows experiment header
    def show(self):
        '''Print out crystallization space data.'''
        print '==== Xpace ===='
        print 'Prot. code:',self.pc,'Long:',self.pn
        print 'Exp. code :',self.ec,'Long:',self.en
        print len(self.dim),'dimensions:',self.dim
        print 'Units:',self.unit
##  Shows experiment numbers
    def show_exp(self):
        '''Print out experiments and observations.'''
        print '==== Xperiments ===='
        for i in self.exp: print i
        print '==== Obs ===='
        print self.obs
##  Save to file
    def save(self,name,option):
        '''Save Xpace to file with $1=file name, $2=use only 2(text).'''
        fo=open(name,'w')
        if (option==0):
            pass
##            pickle.dump(self,fo)
        elif (option==1):
            fo.write(self.pc)
            fo.write('\n')
            fo.write(self.pn)
            fo.write('\n')
            fo.write(self.ec)
            fo.write('\n')
            fo.write(self.en)
            fo.write('\n')
            fo.write(str(self.dim))
            fo.write('\n')
            fo.write(str(self.unit))
            fo.write('\n')
            fo.write(str(self.exp))
            fo.write('\n')
            fo.write(str(self.obs))
            fo.write('\n')
        elif (option==2):
            fo.write('0 0\n')
            fo.write(self.pc)
            fo.write('\n')
            fo.write(self.pn)
            fo.write('\n')
            fo.write(self.ec)
            fo.write('\n')
            fo.write(self.en)
            fo.write('\n')
            fo.write(str(len(self.dim))) # Number, list od dimensions
            fo.write(' ')
            for i in self.dim:
                fo.write(str(i))
                fo.write(' ')
            fo.write('\n')
            fo.write(str(len(self.unit))) # Number, list of units
            fo.write(' ')
            for i in self.unit:
                fo.write(str(i))
                fo.write(' ')
            fo.write('\n')
            fo.write(str(len(self.exp))) # Number
            for i in self.exp:
                fo.write('\n')
                for j in i:             # Experiment coordinates         
                    fo.write(str(j))
                    fo.write(' ')
            fo.write('\n')
#            fo.write(str(len(self.obs))) # Number, list of observations
            fo.write(' ')
            for i in self.obs:
                fo.write(str(i))
                fo.write(' ')
        elif option==3:
# XML format
          pass
        else: print 'ERROR: Option non supported. File not saved.'
        fo.close()
## Read from file
    def read(self,name, option):
        '''Read experient file.'''
        self.__init__()
        fo=open(name,'r')
        if (option==0):
            pass
        elif (option==1):
            self.pc=fo.readline()
            self.pn=fo.readline()
            self.ec=fo.readline()
            self.en=fo.readline()
            self.dim=list(fo.readline())
            self.units=list(fo.readline())
            self.exp=fo.readline()
            self.obs=fo.readline()
        elif (option==2):
            print 'Reading file as text'
            x=fo.readline()
            x=x.split()
            self.ver=x[0]
            self.evo=x[1]
            print 'Reading Xpace file version:',self.ver,'Evolution:',self.evo
            self.pc=fo.readline()               # Reading protein code
            if self.pc[-1]=='\n': self.pc=self.pc[:-1]
            self.pn=fo.readline()               # Reading protein long name
            if self.pn[-1]=='\n': self.pn=self.pn[:-1]
            self.ec=fo.readline()               # Reading experiment code   
            if self.ec[-1]=='\n': self.ec=self.ec[:-1]
            self.en=fo.readline()               # Reading experiment long name
            if self.en[-1]=='\n': self.en=self.en[:-1]
            x=fo.readline()                     # Reading dimension names
            x=x.split()
            if (len(x)!=int(x[0])+1):
                print 'ERROR: Inconsistent dimension record (',x,')'
                return
            self.dim=[]
            for i in x[1:]: self.dim.append(i)
            x=fo.readline()                     # Read the dimension units
            x=x.split()
            if (len(x)!=int(x[0])+1):
                print 'ERROR: Inconsistent units record'
                return
            self.unit=[]
            for i in x[1:]: self.unit.append(i)
            x=fo.readline()                  # Read the number of experiments
            n1=int(x)
            self.exp=[]
            for i in range(n1):                 # Reading all the experiments
                x=fo.readline()
                x=x.split()
                if (len(x)!= len(self.dim)):
                    print 'ERROR: Inconsistent experiment dimension. ' \
                          'Experiment',i,' not read.'
                else:
                    y=[]
                    for i in x:  y.append(float(i)) # Strings to floats
                    self.exp.append(y)
            x=fo.readline()                     # Reading the observation line
            x=x.split()
            self.obs=[]
            if (len(x)!=len(self.exp)):
                print 'ERROR: Inconsistent number of observations (',len(x),\
                      ',',len(self.exp),')'
            else:
                y=[]
                for i in x: y.append(float(i))
                self.obs=y
        elif option==3:
# XML format
          pass
        else: print 'ERROR: Option non supported. File not read.'
        fo.close()
## Merges two experiments into one.
    def merge(self,xpace2):
        '''Merges on Xpace into other.'''
        print 'Merging two Xpaces'
        for i in range(xpace2.dim):
            if (self.index(xpace2.dim[i])>=0):
                if (self.unit[self.index(xpace2.dim[i])]!=xpace2.unit[i]):
                    print 'ERROR: Xpace dimension with different units. No '\
                          'merged'
                    return
            else: self.add_dimension(xpace2.dim[i].xpace2.unit[i])
        new_exp=[]
        new_obs=[]
        exp=[]
        for i in xpace2.exp:
            for j in self.dim:
                if (xpace2.dim.index[j]>=0): exp.append(i[xpace2.dim.index[j]])
                else:                        exp.append(0)
            new.exp.append(exp)
            exp=[]      
        for i in range(len(new_exp)):
            self.add_exp(new_exp[i])
            self.add_obs(new_obs[i])
## Expermients with negative scoring are not valid.            
    def clean(self):
        '''Eliminates experimenst with neagtive observations.'''
        new_exp=[]
        new_obs=[]
        for i in range(len(self.exp)):
            if self.obs[i]>=0:
                new_exp.append(self.exp[i])
                new_obs.append(float(self.obs[i]))
        print '\n\nCleaning: Initial =',len(self.exp),
        self.exp=new_exp
        self.obs=new_obs
        print 'After =',len(self.exp)
##
    def mom(x,w,moment):
        '''Weighted sum of all data.'''
        res=0.0
        for i in range(len(x)): res=res+pow(x[i],moment)*w[i]
        return res
##
    def mom2(x,y,w,mom1,mom2):
        '''Weighted sum of all data.'''
        res=0.0
        for i in range(len(x)): res=res+pow(x[i],mom1)*pow(y[i],mom2)*w[i]
        return res
##
## The 1-D analysis
## 
    def analysis1D(self,oo=0):
        '''One dimensional analysis of teh crystallization space.'''
        def a_matrix(x,w,dim):
            '''Setting up the A matrix of the equation system.'''
            if   dim==2 : term=3 #Number of terms to calculate for each matrix.
            elif dim==3 : term=5
            sx=[]                                    
            for i in range(term): sx.append(0.0)
            for i in range(len(x)):
                for j in range(len(sx)): sx[j]=sx[j]+pow(x[i],j)*w[i]
            sx.reverse()
            a=[]
            for i in range(dim):
                for j in sx[i:i+dim]: a.append(j)
            return a
        def b_matrix(x,y,w,dim):
            '''Setting up the A matrix of the equation system.'''
            b=[]
            for i in range(dim): b.append(0.0)
            for i in range(len(x)):
                for j in range(len(b)): b[j]=b[j]+y[i]*pow(x[i],j)*w[i]
            b.reverse()
            return b
        def r_factor(a,x,y,w):
            '''Calculates the R factor (residual).'''
            def get_value(x,a):
                res=0.0
                for i in range(len(a)): res=res+a[i]*pow(x,len(a)-i-1)
                return res
            num=0.
            den=0.
            for i in range(len(x)):
                den=den+y[i]*w[i]
                num=num+abs(y[i]-get_value(x[i],a)*w[i])
            return num/den
        def lsq(x,y,w,par):
            '''Least-squares analysis'''
            a=Matrix(a_matrix(x,w,par),[par,par])
            b=b_matrix(x,y,w,par)
            s=MSystem(a,b)
            fparam=s.solve2()
            if len(fparam)==0:
              if oo==1:
                self.outte.insert('end','\nERROR: Determinant value=0','warn') 
              else:     print 'ERROR: Determinant value=0'
              return
            else: return [fparam,r_factor(fparam,x,y,w)]

## Analysis in 1 dimension main body
        self.clean()            # Eliminate bad experiments.
        x1=[]
        x2=[]
        if oo==1: self.outte.appendtext('\n\n============== 1 D analysis')
        else:     print '1 dimension analysis:'
        for i in range(len(self.dim)):
            if oo==1: self.outte.appendtext('\n== Dimension: %s  Unit: %s' % \
                                         (self.dim[i], self.unit[i]))
            else: print '=====>> Dimension:',self.dim[i],'Unit:',self.unit[i]
            val=[]
            val2=[]
            for j in self.exp:
                if (j[i] not in val): val.append(j[i])
                val2.append(j[i])
            val.sort()
            cou=[]
            for j in val: cou.append(val2.count(j))
            ave=[]
            for j in val:
                x=0.0
                for k in self.exp:
                    if (k[i]==j): x=x+self.obs[self.exp.index(k)]
                x=x/cou[val.index(j)]
                ave.append(x)
            if oo==1: self.outte.appendtext( \
              '\n== Values= %s  Counts= %s  Average= %s' % (val,cou,ave) )
            else: print '   Values:',val,' Counts:',cou,'\n   Average:',ave
            x1.append(lsq(val,ave,cou,2))
            if len(val)>=3: x2.append(lsq(val,ave,cou,3))
            else:           x2.append([[0,0,0],0])
        if oo==1: self.outte.appendtext('\n1 dimension analysis table:')
        else:     print '\n1 dimension analysis table:'
        if len(val)>=3:
          if oo==1:
            self.outte.appendtext(\
              '\n\n              2 param ===============  3 param =============='\
              '==========\n  Factor Unit R        x        1      R        x2'\
              '       x       1')
          else: print '              2 param ===============  3 param =============='\
                     '==========\n  Factor Unit R        x        1      R        x2'\
                     '       x       1'
          for i in range(len(self.dim)):
            if oo==1: self.outte.appendtext( \
              '\n%10s %1s %6.3f %8.3f %8.3f %6.3f %8.3f %8.3f %8.3f' %  \
              (self.dim[i],self.unit[i],x1[i][1],x1[i][0][0],x1[i][0][1],\
              x2[i][1],x2[i][0][0],x2[i][0][1],x2[i][0][2]) )
            else: print '%10s %1s %6.3f %8.3f %8.3f %6.3f %8.3f %8.3f %8.3f' %  \
              (self.dim[i],self.unit[i],x1[i][1],x1[i][0][0],x1[i][0][1],\
              x2[i][1],x2[i][0][0],x2[i][0][1],x2[i][0][2])
        else:
          if oo==1: self.outte.appendtext('\n              2 param ================'\
                  '\n  Factor Unit R        x        1')
          else: print '              2 param ================'\
                  '\n  Factor Unit R        x        1'
          for i in range(len(self.dim)):
            if oo==1: self.outte.appendtext('\n%10s %1s %6.3f %8.3f %8.3f' % \
              (self.dim[i],self.unit[i],x1[i][1],x1[i][0][0],x1[i][0][1]) )  
            else:     print '\n%10s %1s %6.3f %8.3f %8.3f' % \
              (self.dim[i],self.unit[i],x1[i][1],x1[i][0][0],x1[i][0][1])
##
## The 2-D analysis.
##
    def analysis2D(self,opt=0,oo=0):
        '''Analysis in two dimensions'''
        def get_steps(x):
            ''' Get all the possible values along the dimension.'''
            steps=[]
            for i in self.exp:
                if i[x] not in steps: steps.append(i[x])
            steps.sort()
            return steps
        def a_matrix(x,y,w):
            '''Setting up the A matrix of the equation system.'''
            x1=y1=x2=y2=xy=n=0.0
            for i in range(len(x)):
                for j in range(len(y)):
                    x1=x1+x[i]*float(w[i][j])
                    x2=x2+x[i]*x[i]*float(w[i][j])
                    y1=y1+y[j]*float(w[i][j])
                    y2=y2+y[j]*y[j]*float(w[i][j])
                    xy=xy+x[i]*y[j]*float(w[i][j])
                    n=n+float(w[i][j])
            return [x2,xy,x1,xy,y2,y1,x1,y1,n]
        def a2_matrix(x,y,w):
            '''Setting up the A matrix of the equation system.'''
            x1=y1=x2=y2=xy=x2y2=x2y=xy2=n=0.0
            for i in range(len(x)):
                for j in range(len(y)):
                    x1=x1+x[i]*w[i][j]
                    x2=x2+x[i]*x[i]*w[i][j]
                    y1=y1+y[j]*w[i][j]
                    y2=y2+y[j]*y[j]*w[i][j]
                    xy=xy+x[i]*y[j]*w[i][j]
                    x2y=x2y+x[i]*y[j]*x[i]*w[i][j]
                    xy2=xy2+x[i]*y[j]*y[j]*w[i][j]
                    x2y2=x2y2+x[i]*y[j]*x[i]*y[j]*w[i][j]
                    n=n+float(w[i][j])
            return [x2y2,x2y,xy2,xy,x2y,x2,xy,x1,xy2,xy,y2,y1,xy,x1,y1,n]
        def b_matrix(x,y,z,w):
            '''Setting up the A matrix of the equation system.'''
            b=[]
            for i in range(3): b.append(0.0)
            for i in range(len(z)):
                for j in range(len(z[0])):
                    b[0]=b[0]+z[i][j]*x[i]*w[i][j]
                    b[1]=b[1]+z[i][j]*y[j]*w[i][j]
                    b[2]=b[2]+z[i][j]*w[i][j]
            return b
        def b2_matrix(x,y,z,w):
            '''Setting up the A matrix of the equation system.'''
            b=[]
            for i in range(4): b.append(0.0)
            for i in range(len(z)):
                for j in range(len(z[0])):
                    b[0]=b[0]+z[i][j]*x[i]*y[j]*w[i][j]
                    b[1]=b[1]+z[i][j]*x[i]*w[i][j]
                    b[2]=b[2]+z[i][j]*y[j]*w[i][j]
                    b[3]=b[3]+z[i][j]*w[i][j]
            return b            
        def r_factor(a,x,y,z,w):
           '''Calculates the R factor (residual).'''
           def get_value(x,y,a): return x*a[2]+y*a[1]+a[0]
           num=0.
           den=0.
           for i in range(len(z)):
              for j in range(len(z[0])):
                 den=den+z[i][j]*w[i][j]
                 num=num+abs(z[i][j]-get_value(x[i],y[j],a)*w[i][j])
           return num/den
        def r2_factor(a,x,y,z,w):
           '''Calculates the R factor (residual).'''
           def get_value(x,y,a): return x*a[3]+y*a[2]+x*y*a[1]+a[0]
           num=0.
           den=0.
           for i in range(len(z)):
              for j in range(len(z[0])):
                 den=den+z[i][j]*w[i][j]
                 num=num+abs(z[i][j]-get_value(x[i],y[j],a)*w[i][j])
           return num/den
        def lsq2(x,y,z,w):
            '''Least-squares analysis'''
            a=Matrix(a_matrix(x,y,w),[3,3])
            b=b_matrix(x,y,z,w)
            s=MSystem(a,b)
            fparam=s.solve2()
            if len(fparam)==0:
              if oo==1:
                self.outte.insert('end','\nERROR: Determinant value=0','warn') 
              else:     print 'ERROR: Determinant value=0'
              return
            else: return [fparam,r_factor(fparam,x,y,z,w)]
        def lsq22(x,y,z,w):
            '''Least-squares analysis'''
            a=Matrix(a2_matrix(x,y,w),[4,4])
            b=b2_matrix(x,y,z,w)
            s=MSystem(a,b)
            fparam=s.solve2()
            if len(fparam)==0:
              if oo==1:
                self.outte.insert('end','\nERROR: Determinant value=0','warn') 
              else:     print 'ERROR: Determinant value=0'
              return
            else: return [fparam,r2_factor(fparam,x,y,z,w)]
##
## 2-D analysis main body
##
        self.clean()                # Eliminates bad observations
        x=[]
        if oo==1: self.outte.appendtext('2 dimension analysis:')
        else:     print '2 dimension analysis:'
        if opt==1:
            if oo==1:
                self.outte.appendtext('Interations will be taken into account.')
            else: print 'Interations will be taken into account.'
        planes=[]
        for i in range(len(self.dim)):
            for j in range(i+1,len(self.dim)):
                planes.append([self.dim[i],self.dim[j]])
        if oo==1:
          self.outte.appendtext('\nNumber of planes= %s Planes: %s' % \
                                        ( len(planes),planes ) )
        else:
          print ' Number of planes:',len(planes),'Planes:',planes
        for i in planes:
            if oo==1: self.outte.appendtext('\n=====>> Plane:' % (i) )
            else:     print '=====>> Plane:',i
            steps=[get_steps(self.dim.index(i[0])),
                   get_steps(self.dim.index(i[1]))]            
            for j in (0,1):
                if oo==1:
                  self.outte.appendtext('\n  Dimension: %s  Steps: %d = %s'\
                                 % (i[j],len(steps[j]),steps[j]) )
                else:
                  print '  Dimension:',i[j],' Steps',len(steps[j]),'=',steps[j]
            p_count=[]
            p_ave=[]
            temp=[]
            temp2=[]
            for j in steps[0]:
                for k in steps[1]:
                    temp.append(0)
                    temp2.append(0.0)
                p_count.append(temp)
                temp=[]
                p_ave.append(temp2)
                temp2=[]
            for j in self.exp:
                p_count[steps[0].index(j[self.dim.index(i[0])])]\
                       [steps[1].index(j[self.dim.index(i[1])])]=\
                p_count[steps[0].index(j[self.dim.index(i[0])])]\
                       [steps[1].index(j[self.dim.index(i[1])])]+1
                p_ave[steps[0].index(j[self.dim.index(i[0])])]\
                       [steps[1].index(j[self.dim.index(i[1])])]=\
                p_ave[steps[0].index(j[self.dim.index(i[0])])]\
                       [steps[1].index(j[self.dim.index(i[1])])]+\
                       self.obs[self.exp.index(j)]
            bad=0
            for j in range(len(steps[0])):
                for k in range(len(steps[1])):
                    if p_count[j][k]==0:     bad=123
                    elif p_count[j][k]>0:
                        p_ave[j][k]=p_ave[j][k]/p_count[j][k]
            if bad!=0:
              if oo==1:
                self.outte.insert('end', \
                  '\nWARN: not all the plane surface scanned.','warn') 
              else:     print 'WARN: not all the plane surface scanned.'
            if opt==0:
                x.append(lsq2(steps[0],steps[1],p_ave,p_count))
            else:
                x.append(lsq22(steps[0],steps[1],p_ave,p_count))               
        if oo==1: self.outte.appendtext('\n=========Planes======')
        else:
          print '\n 2D analysis table:'
          print '=========Planes======'
        if opt==0:
          if oo==1:
            self.outte.appendtext( \
              '\n      x          y     R        x        y        1')
          else: print '\n      x          y     R        x        y        1'
        else:
          if oo==1:
            self.outte.appendtext( \
              '\n      x          y     R        xy       x        y       1')
          else:
            print '      x          y     R        xy       x        y       1'
        c=0
        for i in planes:
          if opt==0:
            if oo==1:
              self.outte.appendtext('\n%10s %10s %6.3f %8.3f %8.3f %8.3f' % \
                (i[0],i[1],x[c][1],x[c][0][0],x[c][0][1],x[c][0][2]) )
            else:  print '%10s %10s %6.3f %8.3f %8.3f %8.3f' % \
               (i[0],i[1],x[c][1],x[c][0][0],x[c][0][1],x[c][0][2])
          else:
            if oo==1:
              self.outte.appendtext('\n%10s %10s %6.3f %8.3f %8.3f %8.3f %8.3f'\
                % (i[0],i[1],x[c][1],x[c][0][0],x[c][0][1],x[c][0][2], \
                   x[c][0][3]) )
            else: print '%10s %10s %6.3f %8.3f %8.3f %8.3f %8.3f' % \
                (i[0],i[1],x[c][1],x[c][0][0],x[c][0][1],x[c][0][2],x[c][0][3])
          c=c+1  
    def analysisnD(self,opt,oo=0):
        def r_factor(fr,s):
            '''R calculation'''
            r_num=0.0
            r_den=0.0
            for i in xrange(len(self.exp)):
                x=self.obs[i]
                f=s[0]
                for j in xrange(len(fr)):
                    p=s[j+1]
                    for k in fr[j]: p=p*self.exp[i][k]
                    f=f+p
                r_num=r_num+abs(x-f)
                r_den=r_den+x
            return r_num/r_den
        def chi_sq(fr,s):
            '''Chi squared calculation.'''
            r_num=0.0
            for i in xrange(len(self.exp)):
                x=self.obs[i]
                f=s[0]
                for j in xrange(len(fr)):
                    p=s[j+1]
                    for k in fr[j]: p=p*self.exp[i][k]
                    f=f+p
                r_num=r_num+abs(x-f)*abs(x-f)/f
            return r_num
##
        self.clean()
        n=len(self.dim)
        fr=[]       ## Generate lead row
        for i in xrange(n): fr.append([i])
        if oo==1: self.outte.appendtext('\n===================> Full analysis')
        if opt==1:
          if oo==1:
            self.outte.appendtext('\nInteraction will be taken into account.')
          print 'Interaction will be taken into account.'
          for i in xrange(n):
            for j in xrange(i+1,n): fr.append([i,j])
        elif opt==2:
          if oo==1:
            self.outte.appendtext('\nInteraction will be taken into account.')
          print 'Interaction will be taken into account.'
          for i in xrange(n):         fr.append([i,i])            
          for i in xrange(n):
            for j in xrange(i+1,n): fr.append([i,j])                
        inmat=[]    ## Generate index matrix for sums
        for i in fr:
            x=[]
            for j in fr:
                a=copy(j)
                for k in i: a.append(k)
                x.append(a)
            inmat.append(x)
        sfr=[]              ## Initialize sums
        for i in fr: sfr.append(0.0)
        smat=[]
        for i in inmat:
            x=[]
            for j in i: x.append(0.)
            smat.append(x)
        for i in self.exp:  ## Calculate sums
            for j in xrange(len(fr)):
                p=1
                for k in fr[j]: p=p*i[k]
                sfr[j]=sfr[j]+p
                for k in xrange(j,len(fr)):
                    p=1
                    for l in inmat[j][k]: p=p*i[l]
                    smat[j][k]=smat[j][k]+p
        a_mat=[]        ## Build the A matrix of the sytem
        x=[float(len(self.exp))]
        for i in sfr: x.append(i)
        a_mat.append(x)
        for i in xrange(len(sfr)):
            x=[sfr[i]]
            for j in smat[i]: x.append(j)
            a_mat.append(x)
        for i in xrange(len(a_mat[0])):
            for j in xrange(i,len(a_mat[0])): a_mat[j][i]=a_mat[i][j]
        a=Matrix(a_mat)
        b_mat=[]
        for i in xrange(len(fr)+1): b_mat.append(0.0)
        for i in xrange(len(self.exp)):
            b_mat[0]=b_mat[0]+self.obs[i]
            for j in xrange(len(sfr)):
                p=self.obs[i]
                for k in fr[j]: p=p*self.exp[i][k]
                b_mat[j+1]=b_mat[j+1]+p
        s=MSystem(a,b_mat)
        sol=s.solve2()
	if sol!=[]:
          print ' ND analysis table: R=%8.3f chi2=%8.3f' % \
                (r_factor(fr,sol), chi_sq(fr,sol))
          print '===================\nID        =%8.3f' %(sol[0]),
          if oo==1:
            self.outte.appendtext('\n ND analysis table: R=%8.3f chi2=%8.3f' % \
                  (r_factor(fr,sol), chi_sq(fr,sol)) )
            self.outte.appendtext('\n===================\nID        =%8.3f' \
                  %(sol[0]),)
          n_len=len(self.dim)
          point=n_len+1
          if opt==2:
            point=point+n_len
            if oo==1: self.outte.appendtext('  squa  ',)
            else:     print '  squa  ',
          if opt>=1:
            for i in self.dim[1:]:
              if oo==1: self.outte.appendtext('%-8s' % (i),)
              else:     print '%-8s' % (i),
          for i in xrange(n_len):
            if oo==1:
              self.outte.appendtext('\n%-10s=%8.3f' % (self.dim[i],sol[i+1]),)
            else: print '\n%-10s=%8.3f' % (self.dim[i],sol[i+1]),
            if opt==2:
              if oo==1: self.outte.appendtext('%8.3f' % (sol[i+1+n_len]),)
              else:     print '%8.3f' % (sol[i+1+n_len]),
            if opt>=1:
              for j in xrange(i):
                if oo==1: self.outte.appendtext('%-8s' %(''),)
                else:     print '%-8s' %(''),
                for j in xrange(n_len-i-1):
                  if oo==1: self.outte.appendtext('%8.3f' %(sol[j+point]),)
                  else:     print '%8.3f' %(sol[j+point]),  
                point=point+j+1
 	else:
          if oo==1: self.outte.appendtext('\n\nERROR: Undetermined system.')
          print 'ERROR: Undetermined system.'
## Output methods: HTML & LaTeX
    def in_html(self):
      '''HTML output'''
      r='\n<html>\n<h1>Protein code: '+self.pc+'\n<h2>Protein name: ' \
        +self.pn+'\n<h1>Experiment code: '+self.ec+'\n<h2>Experiment name:'\
        +self.en+'\n<p>The crystallization space has '+str(len(self.dim)) \
        +' dimensions:</p>\n<table>'
      for i in range(len(self.dim)):
        r=r+'\n<tr><td>'+str(i)+'</td><td>'+self.dim[i]+' '+self.unit[i]+ \
          '</td></tr>'
      r=r+'\n</table>\n<center><table border="2">\n<tr><td>Experiment</td>'
      for i in self.dim: r=r+'<td>'+i+'</td>'
      r=r+'<td>Quality</td></tr>'
      for i in range(len(self.exp)):
        r=r+'\n<tr><td>'+str(i)+'</td>'
        for j in range(len(self.dim)):
          r=r+'<td>'+str(self.exp[i][j])+' '+self.unit[j]+'</td>'
        r=r+'<td>'+str(self.obs[i])+'</td></tr>'
      r=r+'</table></center>\n<h1>Good luck!</h1></html>'
      r=r+'<p>Xpace %s - Angel Gutierrez Rodriguez</p>' % (xpace_version)
      return r
    def in_xml(self):
      '''XML output'''
      r='<?xml version="1.0" ?>\n<xpace>\n<pr_co>'+self.pc+'</pr_co>\n<pr_na>'\
        +self.pn+'</pr_na>\n<ex_co>'+self.ec+'</ex_co>\n<ex_na>'+self.en \
        +'</ex_na>\n<factors>'
      for i in range(len(self.dim)):
        r=r+'\n<dim><label>'+self.dim[i]+'</label><unit>'+self.unit[i] \
           +'</unit></dim>'
      r=r+'\n</factors>\n<design>'
      for i in range(len(self.exp)):
        r=r+'\n<exp>'
        for j in range(len(self.dim)):
          r=r+'<dv>'+str(self.exp[i][j])+'</dv>'
        r=r+'\n<obs>'+str(self.obs[i])+'</obs></exp>'
      r=r+'</design></xpace>'
      return r
    def in_latex(self):
      '''LaTeX output'''
      r='\n\documentclass[a4paper]{article}\n \\begin{document}\n\nProtein '\
         +'code: {\large '+self.pc+'} Name: '+self.pn+'\n\nExperiment code:'\
         +'{\large '+self.ec+'} Name: '+self.en+'\n\nThe crystallization '\
         +'space has '+str(len(self.dim))+' dimensions:\n\n\\begin{tabular}'\
         +'{rcc}'
      for i in range(len(self.dim)):
        r=r+'\n'+str(i)+' & '+self.dim[i]+' & '+self.unit[i]+'\\\\'
      r=r+'\n\end{tabular}\n\n\\begin{center}\\begin{tabular}{r'
      for i in xrange(len(self.dim)+1): r=r+'|c'
      r=r+'}\hline\n'
      for i in range(len(self.exp)):
        r=r+'\n'+str(i)+' & '
        for j in range(len(self.dim)):
          r=r+str(self.exp[i][j])+' '+self.unit[j]+' & '
        r=r+str(self.obs[i])+'\\\\'
      if (i+1)%5==0 and i>0 and i+1<len(self.exp): r=r+'\hline'
      r=r+'\n\hline\end{tabular}\end{center}\n\n {\large Good luck!}'
      r=r+'\n\nXpace %s - \'Angel Guti\'errez Rodr\'{\i}guez' % (xpace_version)
      r=r+'\end{document}'
      return r
    def open_file1(self): 
      ''' Opens and reads a stock file.''' 
      if self.ifn:
        self.__init__()
        self.read(self.ifn,2) 
        self.pce.setvalue(self.pc)
        self.ece.setvalue(self.ec)
        self.pne.setvalue(self.pn)
        self.ene.setvalue(self.en)
        self.gen_head()
        self.gen_exp()
    def clean_head(self):
      for i in range(len(self.dlwl)):
        self.dlwl[i].destroy()
        self.duwl[i].destroy()
    def clean_exp(self):
      for i in range(len(self.ewl)):
        for j in self.ewl[i]: j.destroy()
        self.fwl[i].destroy()
        self.owl[i].destroy()
    def gen_head(self):
      try:    self.clean_head()
      except: pass
      self.dlwl=[]
      self.duwl=[]
      
      print 'Generates header'
      print self.dim,self.unit

      for i in range(len(self.dim)):
        self.dlwl.append(Pmw.EntryField(self.dimfr.interior(),labelpos='w', \
                         label_text='Label:'))
        self.duwl.append(Pmw.EntryField(self.dimfr.interior(),labelpos='w', \
                     label_text='Unit:',entry_width=2))
        self.dlwl[i].grid(row=i,column=0)
        self.duwl[i].grid(row=i,column=1)
        self.dlwl[i].setvalue(self.dim[i])
        self.duwl[i].setvalue(self.unit[i])
    def makeEntry(self,d): return Pmw.EntryField(self.experfr.interior(), \
                            labelpos='w', label_text='%s=' % (d),entry_width=5)
    def gen_exp(self):
#      try:    self.clean_exp()
#      except: pass
      self.fwl=[]
      self.ewl=[]
      self.owl=[]
      for i in range(len(self.exp)):
        self.fwl.append(Label(self.experfr.interior(),text="Exp %d" % (i)))
        self.fwl[i].grid(row=i,column=0)
        self.ewl.append([self.makeEntry(d) for d in self.dim])
        for j in range(len(self.dim)):
          self.ewl[i][j].grid(row=i,column=j+1)
          self.ewl[i][j].setvalue(self.exp[i][j])
        self.owl.append(Pmw.EntryField(self.experfr.interior(),labelpos='w', \
                     label_text='Obs:',entry_width=5))
        self.owl[i].grid(row=i,column=j+2)
        self.owl[i].setvalue(self.obs[i])
    def coll_head(self):
      self.dim=[]
      self.unit=[]
      for i in range(len(self.dlwl)):
        self.dim.append(self.dlwl[i].getvalue())
        self.unit.append(self.duwl[i].getvalue())
      print self.dim,self.unit
    def coll_exp(self):
      self.exp=[]
      self.obs=[]
      for i in range(len(self.owl)):
        x=[]
        for j in self.ewl[i]: x.append(float(j.getvalue()))
        self.exp.append(x)
        self.obs.append(self.owl[i].getvalue())
    def add(self):
      self.dlwl.append(Pmw.EntryField(self.dimfr.interior(),labelpos='w', \
                    label_text='Label:'))
      self.duwl.append(Pmw.EntryField(self.dimfr.interior(),labelpos='w', \
                    label_text='Unit:',entry_width=2))
      x=len(self.dlwl)-1
      self.dlwl[x].grid(row=x,column=0)
      self.duwl[x].grid(row=x,column=1)
      self.add_dimension('','')
    def dele(self): 
      self.coll_head()
      self.x=Pmw.SelectionDialog(self.dimfr.interior(), \
        title='Delete dimension', buttons=('Delete','Cancel'), \
        defaultbutton='Cancel', scrolledlist_labelpos='n', \
        label_text='Select the dimension to delete:',\
        scrolledlist_items=self.dim, command=self.dele2) 
    def adx(self):
      i=len(self.fwl)
      self.fwl.append(Label(self.experfr.interior(), \
                  text="Exp %d" % (len(self.fwl))))

      self.fwl[i].grid(row=i,column=0)
      self.ewl.append([self.makeEntry(d) for d in self.dim])
      for j in range(len(self.dim)):
          self.ewl[i][j].grid(row=i,column=j+1)
          self.ewl[i][j].setvalue(0.0)
      self.owl.append(Pmw.EntryField(self.experfr.interior(),labelpos='w', \
                     label_text='Obs:',entry_width=5))
      self.owl[i].grid(row=i,column=j+2)
      self.owl[i].setvalue(-1)
    def dele2(self,result): 
      if result=='Delete': 
        a=self.x.getcurselection() 
        b=self.dim.index(a[0]) 
        self.del_dimension(a[0])
        self.x.setlist(self.dim) 
      else: self.x.destroy()
    def delex(self):
      '''Delete experiment window'''
      l=[] 
      for i in xrange(len(self.exp)): l.append(i) 
      self.x=Pmw.SelectionDialog(self.experfr.interior(), \
        title='Delete experiments', buttons=('Delete','Cancel'), \
        defaultbutton='Cancel', scrolledlist_labelpos='n', \
        scrolledlist_items=l, label_text='Select the experiment to delete:', \
        command=self.delex2) 
    def delex2(self,result):
      '''Delete experiment'''
      if result=='Delete': 
        a=self.x.getcurselection() 
        w=a[0] 
        del self.exp[w] 
        del self.obs[w]
      else: pass
      self.gen_exp()
      self.x.destroy() 
    def open_file2(self): 
      self.ifn=tkFileDialog.askopenfilename(initialdir='.', \
                                                initialfile='test.xpa') 
      self.ree.setvalue(self.ifn)
    def set_ifn(self): self.ifn=self.ree.getvalue()
    def set_ofn(self): self.ofn=self.sae.getvalue()
    def set_pfn(self): self.pfn=self.pae.getvalue()
#    def save_file1(self): self.save(self.ofn,2)
    def save_file2(self): 
      self.ofn=tkFileDialog.asksaveasfilename(initialdir='.', \
                                                   initialfile='exp.xpa')
      self.sae.setvalue(self.ofn)
    def print_file1(self):
      f=open(self.pfn,'w')
      if    self.prbb.getvalue()=='HTML':  f.write(self.in_html())         
      elif  self.prbb.getvalue()=='LaTeX': f.write(self.in_latex())
      elif  self.prbb.getvalue()=='XML':   f.write(self.in_xml())
      elif  self.prbb.getvalue()=='Xpace': self.save(self.pfn,2)
      f.close()
    def print_file2(self):
      self.pfn=tkFileDialog.asksaveasfilename(initialdir='.')
    def out_file(self):
      self.outte.appendtext('\n\n--\nAngel Gutierrez Rodriguez')
      self.outte.appendtext('\n Xpace %s  -- EXpace %s' %
                            (xpace_version,expace_vesion))
      self.outte.exportfile(tkFileDialog.asksaveasfilename(initialdir='.'))
    def pcc(self): self.pc=self.pce.getvalue()
    def ecc(self): self.ec=self.ece.getvalue()
    def pnc(self): self.pn=self.pne.getvalue()
    def enc(self): self.en=self.ene.getvalue()
    def e1da(self): self.analysis1D(1)
    def e2da(self):
      if   self.d2cb.getvalue()=='Yes': self.analysis2D(1,1)
      elif self.d2cb.getvalue()=='No':  self.analysis2D(0,1)
    def enda(self): 
      if   self.ndcb.getvalue()=='Yes': self.analysisnD(1,1)
      elif self.ndcb.getvalue()=='No':  self.analysisnD(0,1)
      elif self.ndcb.getvalue()=='With x2':  self.analysisnD(2,1)
    def loref(self,p):
      if p=='Input/Output':
          self.gen_head()
          self.gen_exp()
      elif p=='Experiments':
        self.coll_exp()
    def raref(self,p):
      if p=='Input/Output' or p=='Analysis':
        self.coll_head()
        self.coll_exp()
      elif p=='Experiments':
          self.coll_head()
          self.gen_exp()
    def edit2(self,w):
      w.geometry("500x250+20+20")
      w.title('EXpace %s - Xpace %s - 2006 - Angel Gutierrez Rodriguez' % \
              (expace_version,xpace_version))
      w.lift()
      
      self.dlwl=[]
      self.duwl=[]
      self.fwl=[]
      self.ewl=[]
      self.owl=[]

      nbmain = Pmw.NoteBook(w,raisecommand=self.raref,lowercommand=self.loref)
      nbmain.pack(fill = 'both', expand = 1)
# Input/output page
      io=nbmain.add('Input/Output')

      self.ifn=""
      iog1=Pmw.Group(io,tag_text="Open file")
      iog1.pack(fill='x')

      self.ree=Pmw.EntryField(iog1.interior(),labelpos='w', \
               command=self.open_file1, label_text='File:', \
               modifiedcommand=self.set_ifn)
      self.ree.pack(side='left',fill='x',expand=1)
      rrb=Button(iog1.interior(),text='Browse',command=self.open_file2)
      rrb.pack(side='left')
      rrr=Button(iog1.interior(),text='Open',command=self.open_file1)
      rrr.pack(side='left')

      self.ofn=''
##       iog2=Pmw.Group(io,tag_text="Save file")
##       iog2.pack(fill='x')
##       self.sae=Pmw.EntryField(iog2.interior(),labelpos='w', \
##                value=self.ofn, label_text='File:', modifiedcommand=self.set_ofn)
##       self.sae.pack(side='left',fill='x',expand=1)
##       srb=Button(iog2.interior(),text='Browse',command=self.save_file2)
##       srb.pack(side='left')
##       srr=Button(iog2.interior(),text='Save',command=self.save_file1)
##       srr.pack(side='left')

      iog3=Pmw.Group(io,tag_text="Save Xpace to file")
      iog3.pack(fill='x')
      self.prbb=Pmw.RadioSelect(iog3.interior(), labelpos = 'w', \
                label_text = 'Format:')
      self.prbb.add('Xpace')     
      self.prbb.add('XML')
      self.prbb.add('HTML')
      self.prbb.add('LaTeX')
      self.prbb.pack(side='top', padx = 5, pady = 2)
        
      self.pfn=''
      self.pae=Pmw.EntryField(iog3.interior(),labelpos='w', \
                              command=self.print_file1, \
                              modifiedcommand=self.set_pfn,label_text='File:')
      self.pae.pack(side='left',fill='x',expand=1)
      prb=Button(iog3.interior(),text='Browse',command=self.print_file2)
      prb.pack(side='left')
      prr=Button(iog3.interior(),text='Save',command=self.print_file1)
      prr.pack(side='left')
# Header page
      header=nbmain.add('Header')
      heg1=Pmw.Group(header,tag_text="Header")
      heg1.pack(fill = 'both', expand = 1)

      self.pce=Pmw.EntryField(heg1.interior(),labelpos='w',\
        command=self.pcc,modifiedcommand=self.pcc,label_text='Protein code:')
      self.pce.pack(side='bottom',fill = 'both', expand = 1, padx = 6, pady = 2)
      self.ece=Pmw.EntryField(heg1.interior(),labelpos='w', \
        command=self.ecc,modifiedcommand=self.ecc,label_text='Experiment code:')
      self.ece.pack(side='bottom',fill = 'both', expand = 1, padx = 6, pady = 2)
      self.pne=Pmw.EntryField(heg1.interior(),labelpos='w', \
        command=self.pnc,modifiedcommand=self.pnc,label_text='Protein:')
      self.pne.pack(side='bottom',fill = 'both', expand = 1, padx = 6, pady = 2)
      self.ene=Pmw.EntryField(heg1.interior(),labelpos='w', \
        command=self.enc,modifiedcommand=self.enc,label_text='Experiment:')
      self.ene.pack(side='bottom',fill = 'both', expand = 1, padx = 6, pady = 2)
      dbb=Pmw.ButtonBox(header)
      dbb.add('Add dimension',command=self.add)
      dbb.add('Delete dimension',command=self.dele)
      dbb.pack(fill = 'both',padx=5,pady=5)
      self.dimfr=Pmw.ScrolledFrame(header,label_text='Dimensions:', \
                                   labelpos="nw")
      self.dimfr.pack(fill = 'x', expand = 1)
# Experiments page
      exper=nbmain.add('Experiments')
      ebb=Pmw.ButtonBox(exper)
      ebb.add('Add experiment',command=self.adx)
      ebb.add('Delete experiment',command=self.delex)
      ebb.pack(fill = 'both', expand = 1, padx=2, pady=2)
      self.experfr=Pmw.ScrolledFrame(exper,labelpos='nw', \
                                     label_text='Experiments:')
      self.experfr.pack(fill = 'both', expand = 1, padx=2, pady=2)
# Analysis page
      ana=nbmain.add('Analysis')
      bufr=Frame(ana)
      bufr.pack()
      d1b=Button(bufr,text='One dimension',command=self.e1da,padx=3,pady=3)
      d2b=Button(bufr,text='Two dimensions',command=self.e2da,padx=3,pady=3)
      self.d2cb=Pmw.RadioSelect(bufr,buttontype="radiobutton")
      ndb=Button(bufr,text='Full analysis',command=self.enda,padx=3,pady=3)
      self.ndcb=Pmw.RadioSelect(bufr,buttontype="radiobutton")
      peb=Button(bufr,text='Print to file',command=self.out_file,padx=3,pady=3)
      d1b.grid(row=0,column=0,sticky='w')
      d2b.grid(row=1,column=0,sticky='w')
      Label(bufr,text="Interactions:").grid(row=1,column=1)
      self.d2cb.grid(row=1,column=2,sticky='w')
      self.d2cb.add('Yes')
      self.d2cb.add('No')
      self.d2cb.setvalue('No')
      ndb.grid(row=2,column=0,sticky='w')
      self.ndcb.grid(row=2,column=2,sticky='w')
      Label(bufr,text="Interactions:").grid(row=2,column=1)
      self.ndcb.add('Yes')
      self.ndcb.add('No')
      self.ndcb.setvalue('No')
      self.ndcb.add('With x2')
      peb.grid(row=3,column=0,sticky='w')
      self.outte=Pmw.ScrolledText(ana, \
        text_font=tkFont.Font(family='Courier',size='10'))
      self.outte.pack(fill='both',expand=1)
      self.outte.tag_configure('warn',foreground='red',background='white', \
                               relief='groove')
# About page
      about=nbmain.add('About')
##        f1=Frame(about)
##        f1.pack(side=LEFT)
      f2=Frame(about)
      f2.pack(side=LEFT)
      Label(f2,text='Xpace v 1 e 2', \
            font=tkFont.Font(family='Lucida',size='14')).pack()        
      Label(f2,text='EXpace v 2 e 0',\
            font=tkFont.Font(size='14')).pack()        
      Label(f2,text='Angel Gutierrez Rodriguez',\
            font=tkFont.Font(family='Lucida',size='14')).pack()        
      Label(f2,text='agr@fq.uniovi.es',\
            font=tkFont.Font(family='Courier',size='12')).pack()        
      Label(f2,text='2006').pack()        
      Label(f2,text='SpLine/BM25 - ESRF - Grenoble - France').pack()        
      Label(f2,text='ICMM - CSIC - Madrid - Spain').pack()        
# Exit page
      exit=nbmain.add('Exit')
      Label(exit,font=tkFont.Font(size='14'), text= \
        'Are you sure you want to leave the program? \nDid you save your data?'\
        ).pack()
      ebb=Pmw.ButtonBox(exit,padx=5,pady=5)
      ebb.add('Exit',command=w.destroy,padx=10,pady=10)
      ebb.add('Save to a file',command=self.save_file2,padx=10,pady=10)
      ebb.pack(fill = 'both', expand = 1, padx=2, pady=2)
      
      nbmain.setnaturalsize()
################################################################################
if __name__=='__main__':
## Execution mode
    main=Xpace()
    w=int(raw_input('Window (graphical) system (1-Yes, 0-No)?'))
#    w=1
    if w==0:
## Text mode
        file=raw_input('Experiment file>')
        main.read(file,2)
        ana=int(raw_input('Analysis (1-1D, 2-2D, 0-nD)>'))
        if ana==0:
            win=int(raw_input('Interactions (1-Yes, 2-with x2, 0-No)>'))
            if   win==1: main.analysisnD(1)
            elif win==2: main.analysisnD(2)
            else:        main.analysisnD(0)
        elif ana==2:
            win=int(raw_input('Interactions (1-Yes, 2-No)>'))
            if win==1: main.analysis2D(1)
            else:      main.analysis2D(0)
        else: main.analysis1D()
    else:
## Tkinter window
        root=Tk()
        main.edit2(root)
        root.mainloop()
