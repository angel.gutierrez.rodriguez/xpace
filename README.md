# Xpace

(**Python  2**) Code from:

[Xpace: a Python class to store and analyse crystallization experiment data](https://doi.org/10.1107/S0021889807016342)

Á Gutiérrez-Rodríguez, Santiago García-Granda

_Journal of Applied Crystallography_ (2007), **40**, 615-617

This document presents a Python script designed to store and analyse the outcome of the factorial designs of macromolecular crystallization experiments for X-ray diffraction. The resulting script comprises three classes with all the methods needed to perform the analysis.

Also in [Researchgate](https://www.researchgate.net/publication/230727307_Xpace_A_Python_class_to_store_and_analyse_crystallization_experiment_data)
